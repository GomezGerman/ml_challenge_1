"""Unit tests"""
import unittest
import csv
import os
import configparser
import acreator
import ldap as ldap_reference
from tools import ldap_connector as lconnect
from tools import database
# Unit test must be independent but in this project we are using objects
# generated by other methods, so I'll try to test the generators first (not ok)


class TestACreator(unittest.TestCase):
    """Test class for acreator script and its dependencies"""
    def setUp(self):
        """Initial setup for testing"""
        # Reading config file
        default_config = configparser.ConfigParser()
        default_config.read('config.ini')

        # simple file structure verification
        assert 'TEST' in default_config

        default_cfg = default_config['TEST']
        self.csv_file_path = default_cfg['csv_file']
        self.smtp_server = default_cfg['smtp_server']
        self.smtp_login = default_cfg['smtp_login']
        self.smtp_password = default_cfg['smtp_password']
        self.from_address = default_cfg['from_address']
        self.db_user = default_cfg['mongo_user']
        self.db_password = default_cfg['mongo_password']
        self.db_host = default_cfg['mongo_host']
        self.db_name = default_cfg['mongo_db']
        self.ldap_host = default_cfg['ldap_host']
        self.ldap_bind = default_cfg['ldap_bind']
        self.ldap_password = default_cfg['ldap_password']
        self.ldap_group = default_cfg['new_users_group']
        self.ldap_base = default_cfg['ldap_base']
        self.required_mail = default_cfg['notify_via_email']

        self.user1 = {'First Name': 'name1',
                      'Last Name': 'lname1',
                      'Email': 'mail1@mail.com'}
        self.create_test_csv(self.csv_file_path)

    def tearDown(self):
        """Deletes the dummy csv file at the end of the tests"""
        self.delete_file(self.csv_file_path)

    def create_test_csv(self, csv_name):
        """Creates a dummy csv file for the testing"""
        with open(csv_name, 'w') as csvfile:
            fieldnames = ["First Name", "Last Name", "Email"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(dict((fn, fn) for fn in writer.fieldnames))
            user1 = {'First Name': 'name1',
                     'Last Name': 'lname1',
                     'Email': 'mail1@mail.com'}
            user2 = {'First Name': 'name2',
                     'Last Name': 'lname2',
                     'Email': 'mail2@mail.com'}
            user3 = {'First Name': 'name3',
                     'Last Name': 'lname3',
                     'Email': 'mail3@mail.com'}
            user4 = {'First Name': 'name4',
                     'Last Name': 'lname4',
                     'Email': 'mail4@mail.com'}
            writer.writerow(user1)
            writer.writerow(user2)
            writer.writerow(user3)
            writer.writerow(user4)

    def delete_file(self, file_name):
        """Wrapper for file delete"""
        if os.path.isfile(file_name):
            os.remove(file_name)
        else:
            print("The file {0} does not exists".format(file_name))

    def test_read_input_file(self):
        """Simple test of reading the dummy csv file"""
        result = acreator.read_input_file(self.csv_file_path)
        lnames = []
        for _, row in result.iterrows():
            lname = row["Last Name"]
            lnames.append(lname)

        self.assertEqual(lnames, ["lname1", "lname2", "lname3", "lname4"])

    def test_ldap_connection(self):
        """Tests an ldap connection and validates the output of the method"""
        ldap = lconnect.LdapConnect(self.ldap_host,
                                    self.ldap_base,
                                    self.ldap_bind,
                                    self.ldap_password,
                                    self.ldap_group)
        result = ldap.connect()
        self.assertIsInstance(result,
                              ldap_reference.ldapobject.SimpleLDAPObject)

    def test_generate_password(self):
        """Tests the password generation (len, hash format, etc)"""
        password, hashed = acreator.generate_password()
        self.assertEqual(len(password), 8)
        # Regex match looking for heading {SHA} and 27 chars
        self.assertRegexpMatches(hashed, r'\{SHA\}(?:\b.{27}\b)+')

    def test_create_account(self):
        """This tests creates a dummy ldap account, it also tests search"""
        ldap = lconnect.LdapConnect(self.ldap_host,
                                    self.ldap_base,
                                    self.ldap_bind,
                                    self.ldap_password,
                                    self.ldap_group)
        _, hashed = acreator.generate_password()

        search_result = ldap.search_account(self.user1['Email'])
        self.assertIsInstance(search_result, list)
        self.assertEqual(len(search_result), 0)

        ldap.create_account(self.user1['First Name'],
                            self.user1['Last Name'],
                            self.user1['Email'],
                            hashed)

        search_result = ldap.search_account(self.user1['Email'])
        self.assertIsInstance(search_result, list)
        self.assertEqual(len(search_result), 1)
        dn_ = search_result[0][0]

        ldap.delete_account(dn_)
        search_result = ldap.search_account(self.user1['Email'])
        self.assertEqual(len(search_result), 0)

    def test_store_user(self):
        """Tests the user stores in the database"""
        mdb = database.MongoDb(self.db_host, self.db_name,
                               self.db_user, self.db_password)
        _, hashed = acreator.generate_password()

        mdb.insert_user(self.user1['First Name'],
                        self.user1['Last Name'],
                        self.user1['Email'],
                        hashed,
                        True)
        # quering stored user
        result = mdb.search_user_by_email(self.user1['Email'])
        found = []
        for i in result:
            found.append(i)

        self.assertEqual(len(found), 1)
        self.assertEqual(found[0]['email'], self.user1['Email'])

        result = mdb.remove_user_by_email(self.user1['Email'])
        result = mdb.search_user_by_email(self.user1['Email'])
        found = []
        for i in result:
            found.append(i)
        self.assertEqual(len(found), 0)

    def test_email_connection(self):
        """Test if the smtp connection was possible
        (if notifications are required)"""
        if self.required_mail == "yes":
            mail_server = gmail_connector.Server(self.smtp_server,
                                                 self.smtp_login,
                                                 self.smtp_password,
                                                 self.from_address)
            self.assertIsInstance(mail_server.server, smtplib.SMTP)


