"""Mail Connector"""
import smtplib


class Server():
    """Mail class for smpt server connection"""

    def sendmail(
            self,
            to_addr_list,
            cc_addr_list,
            subject, message):
        """Sends email, it accepts lists as destinations"""
        header = 'From: %s\n' % self.from_address
        header += 'To: %s\n' % ','.join(to_addr_list)
        header += 'Cc: %s\n' % ','.join(cc_addr_list)
        header += 'Subject: %s\n\n' % subject
        message = header + message

        # here I can verify email address but usually VRFY is disabled
        problems = self.server.sendmail(self.from_address,
                                        to_addr_list, message)
        # server.quit()
        return problems

    def quit_server(self):
        """Closes the servers connection"""
        self.server.quit()

    def __init__(self, smtp_server, smtp_login, smtp_password, from_address):
        """Initialization of the server class with the login data"""

        self.from_address = from_address
        self.server = smtplib.SMTP(smtp_server)
        self.server.starttls()
        self.server.login(smtp_login, smtp_password)
