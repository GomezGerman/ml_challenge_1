"""Database connectors, contains the connectos for specifics databases"""
import datetime
import urllib
from pymongo import MongoClient


class MongoDb():
    """MongoDb handling class, following the same model, it is possible to add
    connectors for other NoSql databases"""
    def __init__(self, host, db_name, user=None, password=None):
        """Initialization of the mongodb class"""
        if not user:
            self.client = MongoClient(host)
        else:
            username = urllib.quote_plus(user)
            password = urllib.quote_plus(password)
            connect_string = host % (username, password)
            self.client = MongoClient(connect_string)
        self.mongo_db = self.client[db_name]

    def insert_user(self, fname, lname, email, hashed_password, created_state):
        """Simple wrapper for user account insert to the database."""

        user = {"first name": fname,
                "last name": lname,
                "email": email,
                "encrypted_pass": hashed_password,
                "date": datetime.datetime.utcnow(),
                "is_created": created_state}

        users = self.mongo_db.users
        user_id = users.insert_one(user).inserted_id
        return user_id

    def search_user_by_email(self, email):
        """query user by email"""
        users = self.mongo_db.users
        return users.find({"email": email})

    def remove_user_by_email(self, email):
        """queries user by email then deletes it"""
        users = self.mongo_db.users
        return users.remove({"email": email})
