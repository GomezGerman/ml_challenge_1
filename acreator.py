#!/usr/bin/python
"""Acreator: LDAP Account creation from .csv file"""
import configparser
import sha
from base64 import b64encode
from tools import mail_connector as gmail_connector
from tools import database
from tools import rand_pass_gen as randompass
from tools import ldap_connector as lconnect
import pandas as pd  # pandas data analytics


def main():
    """Main script excecution path, starts by reading the config file"""
    # Reading config file
    default_config = configparser.ConfigParser()
    default_config.read('config.ini')

    assert 'PROD' in default_config  # simple file structure verification

    default_cfg = default_config['PROD']
    csv_file_path = default_cfg['csv_file']
    smtp_server = default_cfg['smtp_server']
    smtp_login = default_cfg['smtp_login']
    smtp_password = default_cfg['smtp_password']
    from_address = default_cfg['from_address']
    db_user = default_cfg['mongo_user']
    db_password = default_cfg['mongo_password']
    db_host = default_cfg['mongo_host']
    db_name = default_cfg['mongo_db']
    ldap_host = default_cfg['ldap_host']
    ldap_base = default_cfg['ldap_base']
    ldap_bind = default_cfg['ldap_bind']
    ldap_password = default_cfg['ldap_password']
    ldap_group = default_cfg['new_users_group']

    # Do we need to send email notifications?
    if default_cfg['notify_via_email'] == "yes":
        mail_server = gmail_connector.Server(smtp_server,
                                             smtp_login, smtp_password,
                                             from_address)

    # get the csv data as a data_frame
    data_frame = read_input_file(csv_file_path)

    # initializing ldap, db
    ldap = lconnect.LdapConnect(ldap_host, ldap_base, ldap_bind,
                                ldap_password, ldap_group)
    mongo_db = database.MongoDb(db_host, db_name, db_user, db_password)

    for _, row in data_frame.iterrows():
        password, hashed_password = generate_password()
        fname = row["First Name"]
        lname = row["Last Name"]
        email = row["Email"]

        # User creation
        try:
            result = ldap.create_account(fname,
                                         lname,
                                         email,
                                         hashed_password)
        except lconnect.ldap.ALREADY_EXISTS:
            # If the user exits I get the stored password(secure)
            stored_user = ldap.search_account(email)[0][1]
            if 'userPassword' in stored_user:
                hashed_password = stored_user['userPassword'][0]
            result = True
        except:  # requires a little more specifications on ldap errors
            result = False

        if result:
            # Send email with user and password, (if email feature is enabled)
            if 'mail_server' in locals():
                subject, message = parse_email_data(fname, lname,
                                                    email, password)

                mail_server.sendmail([email], [], subject, message)
            # Stores user with hashed_password if the user was already created
            # in ldap directory, a new entry is added to the database as a
            # reviewed record with new date time

            mongo_db.insert_user(fname, lname, email, hashed_password, True)
        else:
            # Store user with creation status False
            mongo_db.insert_user(fname, lname,
                                 email, hashed_password, False)

    if 'mail_server' in locals():
        mail_server.quit_server()

    print("""\n\n ### The script finished ###
    users from csv file were stored in the database with the corresponding
    status about the ldap creation \n\n""")


def read_input_file(file_path):
    """reads the csv file easily with pandas and returns a data_frame.
    In case of huge CSV files, I will need to start thinking on generators,
    pandas dev version does work with generators so maybe that is a good
    starting point"""
    # get csv data as pandas DataFrame structure
    data_frame = pd.read_csv(file_path, sep=',')
    # would be ok to have a csv format validation
    return data_frame


def generate_password():
    """References in randPassGen.py file for specific generation parameters"""
    password = randompass.generate()
    ctx = sha.new(password)
    hash_ = "{SHA}" + b64encode(ctx.digest())
    return password, hash_


def parse_email_data(fname, lname, email, password):
    """Email body and subject parser"""
    subject = "Hi {0} Your new account was created".format(fname)
    message = """Hi {0} {1}:
        Your new account was created.
        username: {2}
        password: {3}

        You will be prompted to change your password
        during your first login""".format(fname, lname,
                                          email, password)
    return subject, message


if __name__ == "__main__":
    main()
