# Acreator: LDAP Account creation from .csv file

## What is Acreator?
Acreator is a Python script which reads users data from a csv file, then creates the ldap accounts, notify the user via email and finally it stores the user status in a NoSql data base.

### Assumptions
* There is an ldapserver and a MongoDb, both accesible from the host running the script.
* There is an email address from an smtp server that accepts the conecction from third party apps/scripts.
* If the provided email account is a gmail account, it needs to turn [Allow less secure apps](https://myaccount.google.com/security) on.
* The users will be created in ldap with email address as the UID
* The password is stored in the database as encrypted (SHA)

### Installation

Acreator requires: [pipenv](https://github.com/pypa/pipenv) to get the environment in which the scripts runs.
If you're on MacOS, you can install Pipenv easily with Homebrew:
```
$ brew install pipenv
```

If you're using Ubuntu 17.10:
```
$ sudo apt install software-properties-common python-software-properties
$ sudo add-apt-repository ppa:pypa/ppa
$ sudo apt update
$ sudo apt install pipenv
```

Or, if you're using Fedora 28:
```
$ sudo dnf install pipenv
```
Otherwise, just use pip:
```
$ pip install pipenv
```

### Clone the repo
```
$ git clone git@bitbucket.org:GomezGerman/ml_challenge_1.git
$ cd ml_challenge_1
```

### Instance the environment and install the dependencies
```
$ pipenv shell
$ pipenv install
```

### Set the configuration file
Acreator gets the configuration from a centralized file.
```sh
$ cp sample_config.ini config.ini 
$ vi config.ini
```
### **config.ini** reference
```
#config.ini

# csv file path (relative to acreator.py)
csv_file = account_data.csv

# ldap base distinguished name
ldap_base = dc=test,dc=lan

# ldap host IP
ldap_host = ldap://127.0.0.1

# ldap admin/manager DN
ldap_bind = cn=Manager,dc=test,dc=lan

# ldap admin/manager password
ldap_password = 123abc

# ldap group for the new accounts to be created
new_users_group = People

# yes/no will determine whether an email notification is required or not
notify_via_email = no

# If email notification was selected, then below lines are required
smtp_server = smtp.gmail.com:587
smtp_login = test@gmail.com
smtp_password = 123abc

# Address from where the notifications will came from
from_address = test@gmail.com

# MongoDb data
mongo_host = mongodb://localhost:27017/
mongo_db = test_db
# If the authentication in case of authentication enabled in the db
mongo_user = 
mongo_password =
```

### CSV format
account_data.csv
```
First Name,Last Name,Email
Foo,Figthers,ffighters@rock.com
...
...
...
```

### Usage
The usage is simple, once inside the environmetn (pipenv shell), just run:
```
$ python acreator.py

 ### The script finished ###
     users from csv file were stored in the database with the corresponding
         status about the ldap creation
```

### Tests
For testing, you need the TEST section in the config.ini file
```
$ python -m unittest -v test.test_acreator
```

### TODO
- Docker compose for testing
- Validate the config.ini file entries, also check for malformed file, format, etc.
- Validate emails format from the csv
- Move passwords to environment variables instead of a file
- **Painpoint:** Considering the CSV could be huge, it is important to implement chunks or another alternative in order to save memory and process. If the local storage is not enough for the file, would be key to read it online as chunks or streaming
- **Painpoint:** If the user already exists on ldap, I don't know, nor have record if the password was already changed, need to reseach on this
- More details for the exceptions such ldap, mongodb, email invalid users, etc.
- Creation of non-happy-path tests
- For the users that were not successfully added to ldap, find out why, and retry in case of necessary

### Author contact
**Gomez Germán** - gomezgerm@gmail.com
