"""Ldap connect"""
import ldap
import ldap.modlist  # required for data format


class LdapConnect():
    """ldap connect class for insert, query and delete users"""
    def __init__(self, host, base, bind_, password, users_group):
        """Initialization of the LdapConnect class, it creates a connection"""
        self.users_group = users_group
        self.base = base
        self.host = host
        self.bind_ = bind_
        self.password = password
        self.connection = self.connect()

    def create_account(self, fname, lname, email, sha_password):
        """Creats and ldap account"""
        dn_ = "uid={0},ou={1},{2}".format(email,
                                          self.users_group,
                                          self.base)
        modlist = {
            "objectClass": ["inetOrgPerson", "shadowAccount"],
            "uid": [email],
            "sn": [lname],
            "givenName": [fname],
            "cn": [fname + " " + lname],
            "shadowLastChange": ["0"],
            "userPassword": [sha_password]
            }
        # shadowLastChange = 0 indicates to request password change on first
        # bind pwdMustChange policy is required to be set on Ldap Server
        # https://tools.ietf.org/id/draft-behera-ldap-password-policy-10.html
        # section.5.2.15

        result = self.connection.add_s(dn_, ldap.modlist.addModlist(modlist))
        if result:
            return result
        return False

    def search_account(self, email):
        """queries an account by its email(UID)"""
        query = "(uid={0})".format(email)
        result = self.connection.search_s(self.base,
                                          ldap.SCOPE_SUBTREE,
                                          query)
        return result

    def connect(self):
        """creates the ldap connection using the user and password provided"""
        connection = ldap.initialize(self.host)
        connection.simple_bind_s(self.bind_, self.password)
        return connection

    def delete_account(self, delete_dn):
        """Deletes an account searching it by DN"""
        result = self.connection.delete_s(delete_dn)
        return result
